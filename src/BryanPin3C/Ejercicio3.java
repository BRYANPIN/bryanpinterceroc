package BryanPin3C;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args){
        double precio, total=0;
        Scanner input=new Scanner(System.in);
        int n = 0;
        do{
            do{
                System.out.print("Ingresar cantidad vendida: ");n=input.nextInt();
                if(n<0)
                    System.out.println("Cantidad no valida");
            }while(n<0);
            if (n>0){
                System.out.print("Ingresar el precio: ");
                do{
                    precio=input.nextInt();
                    if(precio<0) 
                        System.out.println("Precio no valido"); 
                    else total=n*precio;
                }while(precio<0);
            }
        }while(n!=0); 
        System.out.print("Total  = "+total);
   }
}
